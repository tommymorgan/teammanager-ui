FROM node:6.2
WORKDIR /home/teammanager-ui
ADD . .
EXPOSE 8088
CMD ["npm","start"]
