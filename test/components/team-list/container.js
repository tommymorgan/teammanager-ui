import { equals } from "ramda";
import { expect } from "chai";
import { List } from "immutable";
import {
    mapStateToProps,
    Teams,
} from "../../../src/components/team-list/container";
import { TeamRecord } from "../../../src/records";

describe("<TeamList/> container", function() {
    it("mapStateToProps maps teams", () => {
        const state = {
            teams: List(new TeamRecord({
                id: 1,
                name: "foo",
            })),
        };

        expect(equals(mapStateToProps(state), {
            teams: state.teams.toJS()
        })).to.be.true;
    });
});
