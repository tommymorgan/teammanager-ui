import { expect } from "chai";
import { equals } from "ramda";
import { List } from "immutable";
import Season from "../../../src/components/season/season";
import Teams from "../../../src/components/season/teams";
import { shallow } from "enzyme";
import React from "react";
import { SeasonRecord } from "../../../src/records";
import CreateGame from "../../../src/components/game/create/create";

describe("<Season/>", function() {
    const season = new SeasonRecord({
        id: 1,
        createGame: {
            id: null,
            awayTeamId: 1,
            homeTeamId: 2,
        },
        name: "foo",
        teams: [{
            id: 1,
            name: "Skategoats",
        },{
            id: 2,
            name: "Borrachos Machos",
        }],
    });
    const wrapper = shallow((
        <Season
            season={season.toJS()}
            onAwayTeamChange={() => {}}
            onCreate={() => {}}
            onHomeTeamChange={() => {}}
        />
    ));

    it("renders the season name", () => {
        expect(wrapper.find("h1").text()).to.equal(season.name);
    });

    it("renders the teams", () => {
        const teams = wrapper.find(Teams);
        expect(teams.length).to.equal(1);
        expect(equals(teams.props().teams, season.toJS().teams)).to.be.true;
    });

    it("renders create game section", () => {
        expect(wrapper.find(CreateGame).length).to.equal(1);
    });
});
