import { expect } from "chai";
import Teams from "../../../src/components/season/teams";
import { shallow } from "enzyme";
import React from "react";
import { TeamRecord } from "../../../src/records";

describe("<Teams/>", function() {
    const team1 = new TeamRecord({
        id: 1,
        name: "foo",
    });
    const team2 = new TeamRecord({
        id: 2,
        name: "bar",
    });
    const teams = [team1, team2];
    let wrapper = shallow(<Teams teams={teams}/>);

    it("renders correctly with teams", () => {
        expect(wrapper.find("li").length).to.equal(2);
    });

    it("renders correctly with no teams", () => {
        wrapper = shallow(<Teams/>);
        expect(wrapper.find("li").length).to.equal(1);
    });
});
