import { equals } from "ramda";
import { expect } from "chai";
import { List } from "immutable";
import {
    createGame,
    getSeason,
} from "../../../src/components/season/api";
import { SeasonRecord } from "../../../src/records";
import * as http from "../../../src/http";
import {
    spy,
    stub,
} from "sinon";

describe("Season API", function() {
    beforeEach(() => {
        spy(http, "get");
        stub(http, "post"); // stub this so we don't actually create records
    });

    afterEach(() => {
        http.get.restore();
        http.post.restore();
    });

    it("getSeason", () => {
        const id = 1;

        getSeason(id);
        expect(http.get.calledWith(`http://teammanager.local/api/seasons/${id}/dto`)).to.be.true;
    });

    it("createGame", () => {
        const awayTeamId = 1;
        const homeTeamId = 2;

        createGame(awayTeamId, homeTeamId);
        expect(http.post.calledWith("http://teammanager.local/api/games", {
            awayTeamId,
            homeTeamId,
        })).to.be.true;
    });
});
