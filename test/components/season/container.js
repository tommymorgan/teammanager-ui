/*eslint-env node, browser, mocha */
import { equals } from "ramda";
import { expect } from "chai";
import {
    mapStateToProps,
    mapDispatchToProps,
    default as Container,
} from "../../../src/components/season/container";
import Season from "../../../src/components/season/season";
import { SeasonRecord } from "../../../src/records";
import { mount } from "enzyme";
import {
    spy,
    stub,
} from "sinon";
import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import * as SeasonActions from "../../../src/redux/actions/season";
import * as SeasonApi from "../../../src/components/season/api";

describe("<Season/> container", function() {
    const stateWithSeason = {
        season: new SeasonRecord({
            id: 1,
            createGame: {
                awayTeamId: 1,
                homeTeamId: 2,
            },
            name: "foo",
            teams: [],
        })
    };

    const stateWithoutSeason = {
        season: new SeasonRecord(),
    };

    it("mapStateToProps maps season", () => {
        expect(equals(mapStateToProps(stateWithSeason), {
            season: stateWithSeason.season.toJS()
        })).to.be.true;
    });

    describe("mounted", () => {
        describe("with season", () => {
            let getSeason;
            let wrapper;
            let store;
            let onAwayTeamChange;
            let onHomeTeamChange;
            let onCreateGame;

            beforeEach(() => {
                const reducer = () => stateWithSeason;

                store = createStore(reducer);
                stub(store, "dispatch").returns(() => {});
                stub(SeasonActions, "getSeason").returns(() => {});
                getSeason = spy();
                onAwayTeamChange = spy();
                onHomeTeamChange = spy();
                onCreateGame = spy();

                wrapper = mount((
                    <Provider
                        store={store}
                    >
                        <Container
                            getSeason={getSeason}
                            onAwayTeamChange={onAwayTeamChange}
                            onCreateGame={onCreateGame}
                            onHomeTeamChange={onHomeTeamChange}
                            params={{
                                id: 1
                            }}
                        />
                    </Provider>
                ));
            });

            afterEach(() => {
                store.dispatch.restore();
                getSeason = null;
                wrapper = null;
                SeasonActions.getSeason.restore();
            });

            it("renders the season when there is a valid season", () => {
                expect(wrapper.find(Season).length).to.equal(1);
            });

            it("calls getSeason in componentWillMount", () => {
                expect(store.dispatch.called).to.be.true;
            });

            it("onAwayTeamChange", () => {
                const dispatch = spy();
                const mapped = mapDispatchToProps(dispatch);
                const event = {
                    target: {
                        value: 1,
                    },
                };

                mapped.onAwayTeamChange(event);
                expect(dispatch.getCall(0).calledWith(SeasonActions.setAwayTeamId(event.target.value))).to.be.true;
            });

            it("onHomeTeamChange", () => {
                const dispatch = spy();
                const mapped = mapDispatchToProps(dispatch);
                const event = {
                    target: {
                        value: 1,
                    },
                };

                mapped.onHomeTeamChange(event);
                expect(dispatch.getCall(0).calledWith(SeasonActions.setHomeTeamId(event.target.value))).to.be.true;
            });

            it("onCreateGame", () => {
                const dispatch = spy();
                stub(SeasonApi, "createGame");
                const mapped = mapDispatchToProps(dispatch);
                const awayTeamId = 1;
                const homeTeamId = 2;

                mapped.onCreateGame(awayTeamId, homeTeamId)();
                expect(SeasonApi.createGame.calledWith(awayTeamId, homeTeamId)).to.be.true;
                expect(dispatch.callCount).to.equal(0);
            });
        });

        describe("without season", () => {
            let getSeason;
            let wrapper;
            let store;
            let onAwayTeamChange;
            let onHomeTeamChange;
            let onCreateGame;

            beforeEach(() => {
                const reducer = () => stateWithoutSeason;

                store = createStore(reducer);
                stub(store, "dispatch").returns(() => {});
                stub(SeasonActions, "getSeason").returns(() => {});
                getSeason = spy();
                onAwayTeamChange = spy();
                onHomeTeamChange = spy();
                onCreateGame = spy();

                wrapper = mount((
                    <Provider
                        store={store}
                    >
                        <Container
                            getSeason={getSeason}
                            onAwayTeamChange={onAwayTeamChange}
                            onCreateGame={onCreateGame}
                            onHomeTeamChange={onHomeTeamChange}
                            params={{
                                id: 1
                            }}
                        />
                    </Provider>
                ));
            });

            afterEach(() => {
                store.dispatch.restore();
                getSeason = null;
                wrapper = null;
                SeasonActions.getSeason.restore();
            });

            it("renders null season when there isn't a valid season", () => {
                expect(wrapper.find(Season).length).to.equal(0);
            });
        });
    });
});
