import { expect } from "chai";
import TeamManager from "../../../src/components/team-manager/team-manager";
import { shallow } from "enzyme";
import React from "react";

describe("<TeamManager/>", function() {
    const wrapper = shallow((
        <TeamManager>
            <div className="foo"></div>
        </TeamManager>
    ));

    it("renders children", () => {
        expect(wrapper.find(".foo").length).to.equal(1);
    });
});
