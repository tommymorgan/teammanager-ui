import { equals } from "ramda";
import { expect } from "chai";
import { List } from "immutable";
import {
    mapDispatchToProps,
    mapStateToProps,
    default as TeamManager,
} from "../../../src/components/team-manager/container";
import { Provider } from "react-redux";
import React from "react";
import { createStore } from "redux";
import { SeasonRecord } from "../../../src/records";
import {
    mount,
    shallow,
} from "enzyme";
import { spy } from "sinon";

describe("<TeamManager/> container ()", function() {
    describe("shallow bits", () => {
        const store = createStore(state => state);

        const wrapper = shallow((
            <Provider store={store}>
                <TeamManager>
                    <div></div>
                </TeamManager>
            </Provider>
        ));

        it("renders children", () => {
            expect(wrapper.find("div").length).to.equal(1);
        });
    });
});
