import { expect } from "chai";
import { List } from "immutable";
import SeasonList from "../../../src/components/season-list/season-list";
import { shallow } from "enzyme";
import React from "react";
import { SeasonRecord } from "../../../src/records";

describe("<SeasonList/>", function() {
    const season1 = new SeasonRecord({
        id: 1,
        name: "foo",
    });
    const season2 = new SeasonRecord({
        id: 2,
        name: "bar",
    });
    const seasons = [season1, season2];
    let wrapper = shallow(<SeasonList seasons={seasons}/>);

    it("renders correctly with seasons", () => {
        expect(wrapper.find("li").length).to.equal(2);
    });

    it("renders correctly with no seasons", () => {
        wrapper = shallow(<SeasonList/>);
        expect(wrapper.find("li").length).to.equal(1);
    });
});
