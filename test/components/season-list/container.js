import { equals } from "ramda";
import { expect } from "chai";
import { List } from "immutable";
import {
    mapStateToProps,
    default as Container,
} from "../../../src/components/season-list/container";
import SeasonList from "../../../src/components/season-list/season-list";
import { SeasonRecord } from "../../../src/records";
import { createStore } from "redux";
import * as SeasonsActions from "../../../src/redux/actions/seasons";
import {
    spy,
    stub,
} from "sinon";
import { mount } from "enzyme";
import React from "react";
import { Provider } from "react-redux";

describe("<SeasonList/> container", function() {
    const state = {
        seasons: List(new SeasonRecord({
            id: 1,
            name: "foo",
        })),
    };

    it("mapStateToProps maps season", () => {
        expect(equals(mapStateToProps(state), {
            seasons: state.seasons.toJS()
        })).to.be.true;
    });

    describe("mounted", () => {
        let getSeasons;
        let wrapper;
        let store;

        beforeEach(() => {
            const reducer = () => state;

            store = createStore(reducer);
            stub(store, "dispatch").returns(() => {});
            stub(SeasonsActions, "getSeasons").returns(() => {});
            getSeasons = spy();

            wrapper = mount((
                <Provider
                    store={store}
                >
                    <Container
                        getSeasons={getSeasons}
                    />
                </Provider>
            ));
        });

        afterEach(() => {
            getSeasons = null;
            wrapper = null;
            SeasonsActions.getSeasons.restore();
        })

        it("componentWillMount", () => {
            expect(store.dispatch.called).to.be.true;
        });

        it("render", () => {
            expect(wrapper.find(SeasonList).length).to.equal(1);
        });
    });
});
