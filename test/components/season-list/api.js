import { equals } from "ramda";
import { expect } from "chai";
import { List } from "immutable";
import { getSeasons } from "../../../src/components/season-list/api";
import { SeasonRecord } from "../../../src/records";
import * as http from "../../../src/http";
import { spy } from "sinon";

describe("Season API", function() {
    beforeEach(() => {
        spy(http, "get");
    });

    afterEach(() => {
        http.get.restore();
    });

    it("getSeason", () => {
        const id = 1;

        getSeasons(id);
        expect(http.get.calledWith("http://teammanager.local/api/seasons")).to.be.true;
    });
});
