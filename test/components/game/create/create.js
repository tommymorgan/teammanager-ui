import { expect } from "chai";
import { shallow } from "enzyme";
import React from "react";
import CreateGame from "../../../../src/components/game/create/create";
import { spy } from "sinon";

describe("<CreateGame/>", function() {
    let onAwayTeamChange = spy();
    let onHomeTeamChange = spy();
    let onCreate = spy();
    let teams = [{
        id: 1,
        name: "foo",
    }, {
        id: 2,
        name: "bar",
    }];
    let wrapper = shallow((
        <CreateGame
            awayTeamId={1}
            homeTeamId={2}
            onAwayTeamChange={onAwayTeamChange}
            onHomeTeamChange={onHomeTeamChange}
            onCreate={onCreate}
            teams={teams}
        />
    ));

    beforeEach(() => {
    });

    afterEach(() => {
    });

    it("renders 2 dropdown lists", () => {
        expect(wrapper.find("select").length).to.equal(2);
    });

    it("calls onCreate callback with selected team ids", () => {
        wrapper.find("button").simulate("click");
        expect(onCreate.called).to.be.true;
        expect(onCreate.getCall(0).calledWith(1, 2)).to.be.true;
    });
});
