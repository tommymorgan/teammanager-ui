import {
    expect,
} from "chai";
import Index from "../src/index";
import {
    Provider,
} from "react-redux";
import {
    shallow,
} from "enzyme";
import React from "react";
import {
    browserHistory,
    IndexRoute,
    Route,
    Router,
} from "react-router";

describe("<Index/>", function() {
    const wrapper = shallow(<Index/>);

    it("should have a store", () => {
        expect(wrapper.find(Provider).props().store).to.be.defined;
    });

    it("should handle desired routes", () => {
        expect(wrapper.find(IndexRoute).length).to.equal(1);

        const routes = wrapper.find(Route);
        expect(routes.length).to.equal(4);

        const hasPath = path => routes.someWhere(r => r.props().path === path);
        expect(hasPath("/")).to.be.true;
        expect(hasPath("season/:id/players")).to.be.true;
        expect(hasPath("season/:id")).to.be.true;
        expect(hasPath("seasons")).to.be.true;
        // expect(hasPath("team")).to.be.true;
    });
});
