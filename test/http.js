import $ from "jquery";
import { expect } from "chai";
import { spy } from "sinon";
import {
    get,
    post,
} from "../src/http";

describe("HTTP module", function() {
    beforeEach(() => {
        spy($, "get");
        spy($, "post");
    });

    afterEach(() => {
        $.get.restore();
        $.post.restore();
    });

    it("should call jquery.get with the given url", () => {
        const url = "foo";

        get(url);
        expect($.get.calledWith(url)).to.be.true;
    });

    it("should call jquery.post with the given url, data, and options", () => {
        const url = "foo";
        const data = {
            id: 1,
            name: "Skategoats",
        };

        post(url, data);
        expect($.post.calledWith({
            url,
            data: JSON.stringify(data),
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
            },
        })).to.be.true;

        post(url);
        expect($.post.calledWith({
            url,
            data: JSON.stringify({}),
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
            },
        })).to.be.true;
    });
});
