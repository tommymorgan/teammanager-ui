import { Deferred } from "jquery";
import { expect } from "chai";
import { equals } from "ramda";
import * as SeasonsActions from "../../../src/redux/actions/seasons";
import {
    spy,
    stub,
} from "sinon";

describe("Seasons actions", function() {
    it("getSeasons", (done) => {
        const response = "foo";
        const apiCall = Deferred();
        const dispatch = spy();

        SeasonsActions.getSeasons(apiCall)(dispatch);
        apiCall.resolve(response);
        apiCall.always(() => {
            expect(dispatch.called).to.be.true;
            expect(dispatch.calledWith(SeasonsActions.setSeasons(response))).to.be.true;
            done();
        });
    });

    it("setSeasons", () => {
        const seasons = "foo";
        expect(equals(SeasonsActions.setSeasons(seasons), {
            type: SeasonsActions.SET_SEASONS,
            seasons,
        })).to.be.true;
    });
});
