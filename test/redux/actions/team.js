import {
    expect,
} from "chai";
import {
    equals
} from "ramda";
import TeamActions from "../../../src/redux/actions/team";

describe("Team actions", function() {
    it("addTeams", () => {
        const teams = "foo";
        expect(equals(TeamActions.addTeams(teams), {
            type: TeamActions.ADD_TEAMS,
            teams,
        })).to.be.true;
    });

    it("getTeamsByPlayer", () => {
        const playerId = "foo";
        expect(equals(TeamActions.getTeamsByPlayer(playerId), {
            type: TeamActions.GET_TEAMS_BY_PLAYER,
            playerId,
        })).to.be.true;
    });

    it("getTeamsBySeason", () => {
        const seasonId = 1;
        expect(equals(TeamActions.getTeamsBySeason(seasonId), {
            type: TeamActions.GET_TEAMS_BY_SEASON,
            seasonId,
        })).to.be.true;
    });

    it("getTeamsByTeam", () => {
        const teamId = 1;
        expect(equals(TeamActions.getTeamsByTeam(teamId), {
            type: TeamActions.GET_TEAMS_BY_TEAM,
            teamId,
        })).to.be.true;
    });
});
