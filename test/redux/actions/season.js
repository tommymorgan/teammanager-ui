import { expect } from "chai";
import $ from "jquery";
import { equals } from "ramda";
import { spy } from "sinon";
import * as SeasonActions from "../../../src/redux/actions/season";
import * as SeasonApi from "../../../src/components/season/api";
import { SeasonRecord } from "../../../src/records";

describe("Season actions", function() {
    let dispatch;
    let apiCall;
    let seasonWith0Teams;
    let seasonWith1Team;
    let seasonWith2Teams;

    beforeEach(() => {
        dispatch = spy();
        apiCall = $.Deferred();
        seasonWith0Teams = new SeasonRecord({
            id: 1,
            createGame: null,
            teams: [],
        });

        seasonWith1Team = new SeasonRecord({
            id: 1,
            createGame: null,
            teams: [{
                id: 1,
                name: "Skategoats",
            }]
        });

        seasonWith2Teams = new SeasonRecord({
            id: 1,
            createGame: null,
            teams: [{
                id: 1,
                name: "Skategoats",
            }, {
                id: 2,
                name: "Borrachos Machos",
            }]
        });
    });

    afterEach(() => {
        dispatch = null;
    });

    it("getSeason (0 teams)", (done) => {
        const season = seasonWith0Teams;
        SeasonActions.getSeason(apiCall)(dispatch);
        apiCall.resolve(season);

        apiCall.done(() => {
            expect(dispatch.callCount).to.equal(1);
            expect(dispatch.calledWith(SeasonActions.setSeason(season))).to.be.true;
            done();
        });
    });

    it("getSeason (1 team)", (done) => {
        const season = seasonWith1Team;
        SeasonActions.getSeason(apiCall)(dispatch);
        apiCall.resolve(season);

        apiCall.done(() => {
            expect(dispatch.callCount).to.equal(1);
            expect(dispatch.calledWith(SeasonActions.setSeason(season))).to.be.true;
            done();
        });
    });

    it("getSeason (2 teams)", (done) => {
        const season = seasonWith2Teams;
        SeasonActions.getSeason(apiCall)(dispatch);
        apiCall.resolve(season);

        apiCall.done(() => {
            expect(dispatch.callCount).to.equal(3);
            expect(dispatch.calledWith(SeasonActions.setSeason(season))).to.be.true;
            expect(dispatch.calledWith(SeasonActions.setAwayTeamId(season.teams[0].id))).to.be.true;
            expect(dispatch.calledWith(SeasonActions.setHomeTeamId(season.teams[1].id))).to.be.true;
            done();
        });
    });

    it("setSeason", () => {
        const season = "foo";
        expect(equals(SeasonActions.setSeason(season), {
            type: SeasonActions.SET_SEASON,
            season
        })).to.be.true;
    });
});
