import { expect } from "chai";
import { List } from "immutable";
import { equals } from "ramda";
import * as SeasonsActions from "../../../src/redux/actions/seasons";
import reducer from "../../../src/redux/reducers/seasons";

describe("Seasons reducer", function() {
    it("SET_SEASONS should return seasons data", () => {
        const seasons = List([{
            id: 1,
            name: "foo",
        }]);
        const state = reducer(undefined, SeasonsActions.setSeasons(seasons));
        const pojoState = state.toJS();

        expect(pojoState.length).to.equal(1);
        expect(equals(pojoState, seasons.toJS())).to.be.true;
    });
});
