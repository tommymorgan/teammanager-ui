import { equals } from "ramda";
import { expect } from "chai";
import * as SeasonActions from "../../../src/redux/actions/season";
import reducer from "../../../src/redux/reducers/season";
import { List } from "immutable";

describe("Season reducer", function() {
    it("SET_SEASON should return season data", () => {
        const season = {
            id: 1,
            name: "foo",
            teams: "teams"
        };
        const state = reducer(undefined, SeasonActions.setSeason(season));
        const pojoState = state.toJS();

        expect(pojoState.id).to.equal(season.id);
        expect(pojoState.name).to.equal(season.name);
        expect(pojoState.teams).to.equal(season.teams);
    });

    it("SET_AWAY_TEAM_ID", () => {
        const state = reducer(undefined, SeasonActions.setAwayTeamId(1));
        const pojoState = state.toJS();

        expect(pojoState.createGame.awayTeamId).to.equal(1);
    });

    it("SET_HOME_TEAM_ID", () => {
        const state = reducer(undefined, SeasonActions.setHomeTeamId(1));
        const pojoState = state.toJS();

        expect(pojoState.createGame.homeTeamId).to.equal(1);
    });
});
