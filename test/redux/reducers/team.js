import {
    expect
} from "chai";
import TeamActions from "../../../src/redux/actions/team";
import reducer from "../../../src/redux/reducers/team";

describe("Team reducer", function() {
    it("ADD_TEAMS should return teams data", () => {
        const teams = [{
            id: 1,
            name: "foo",
        }, {
            id: 2,
            name: "bar",
        }];
        const state = reducer(undefined, TeamActions.addTeams(teams));
        const pojoState = state.toJS();

        expect(pojoState.length).to.equal(2);
    });
});
