/*eslint-env node, browser, mocha */
import {
    applyMiddleware,
    combineReducers,
    createStore,
} from "redux";
import indexReducer from "./redux/reducers/index";
import seasonReducer from "./redux/reducers/season";
import seasonsReducer from "./redux/reducers/seasons";
import teamReducer from "./redux/reducers/team";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";
import {
    browserHistory,
    IndexRoute,
    Route,
    Router,
} from "react-router";
import Season from "./components/season/container";
import SeasonList from "./components/season-list/container";
// import Teams from "./components/redux/teams";
import TeamManager from "./components/team-manager/container";
import {
    routerReducer,
    syncHistoryWithStore,
} from "react-router-redux";
import thunk from "redux-thunk";

const appReducers = combineReducers({
    global: indexReducer,
    routing: routerReducer,
    season: seasonReducer,
    seasons: seasonsReducer,
    teams: teamReducer,
});

const store = createStore(
    appReducers,
    applyMiddleware(thunk)
);

const history = syncHistoryWithStore(browserHistory, store);

const app = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={TeamManager}>
                <IndexRoute component={SeasonList}/>
                <Route path="season/:id/players" component={Season}/>
                <Route path="season/:id" component={Season}/>
                <Route path="seasons" component={SeasonList}/>
                {/*<Route path="team" component={Teams}/>*/}
            </Route>
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById("app"));

export default () => app;
