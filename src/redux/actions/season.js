export const SET_AWAY_TEAM_ID = "SET_AWAY_TEAM_ID";
export const SET_HOME_TEAM_ID = "SET_HOME_TEAM_ID";
export const SET_SEASON = "SET_SEASON";

export const getSeason = apiCall => dispatch => {
    apiCall
        .done((season) => {
            dispatch(setSeason(season));

            const teams = season.teams;
            if (teams.length <= 1) {
                return;
            }

            dispatch(setAwayTeamId(teams[0].id));
            dispatch(setHomeTeamId(teams[1].id));
        });
};

export const setAwayTeamId = id => {
    return {
        type: SET_AWAY_TEAM_ID,
        id,
    };
};

export const setHomeTeamId = id => {
    return {
        type: SET_HOME_TEAM_ID,
        id,
    };
};

export const setSeason = season => {
    return {
        type: SET_SEASON,
        season,
    };
};
