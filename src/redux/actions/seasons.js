export const GET_SEASONS = "GET_SEASONS";
export const SET_SEASONS = "SET_SEASONS";

export const getSeasons = apiCall => dispatch => {
    apiCall
        .then(seasons => dispatch(setSeasons(seasons)));
};

export const setSeasons = (seasons) => {
    return {
        type: SET_SEASONS,
        seasons,
    };
};
