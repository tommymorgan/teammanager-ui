const TeamActions = {
    ADD_TEAMS: "ADD_TEAMS",
    GET_TEAMS_BY_PLAYER: "GET_TEAMS_BY_PLAYER",
    GET_TEAMS_BY_SEASON: "GET_TEAMS_BY_SEASON",
    GET_TEAMS_BY_TEAM: "GET_TEAMS_BY_TEAM",

    addTeams: (teams) => {
        return {
            type: TeamActions.ADD_TEAMS,
            teams,
        };
    },
    getTeamsByPlayer: (playerId) => {
        return {
            type: TeamActions.GET_TEAMS_BY_PLAYER,
            playerId,
        };
    },
    getTeamsBySeason: (seasonId) => {
        return {
            type: TeamActions.GET_TEAMS_BY_SEASON,
            seasonId,
        };
    },
    getTeamsByTeam: (teamId) => {
        return {
            type: TeamActions.GET_TEAMS_BY_TEAM,
            teamId,
        };
    },
};

export default TeamActions;
