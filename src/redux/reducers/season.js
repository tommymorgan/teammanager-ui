import * as SeasonActions from "../actions/season";
import { SeasonRecord } from "../../records";

const initialState = new SeasonRecord();

export default (state = initialState, action) => {
    switch (action.type) {
    case SeasonActions.SET_AWAY_TEAM_ID:
        return state.mergeDeep({
            createGame: {
                awayTeamId: action.id,
            },
        });
    case SeasonActions.SET_HOME_TEAM_ID:
        return state.mergeDeep({
            createGame: {
                homeTeamId: action.id,
            },
        });
    case SeasonActions.SET_SEASON:
        return state.merge(action.season);
    default:
        return state;
    }
};
