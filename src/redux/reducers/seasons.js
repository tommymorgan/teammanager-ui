import * as SeasonActions from "../actions/seasons";
import { List } from "immutable";

const initialState = List([]);

export default (state = initialState, action) => {
    switch (action.type) {
    case SeasonActions.SET_SEASONS:
        return List(action.seasons);
    default:
        return state;
    }
};
