import Actions from "../actions/team";
import {
    List,
} from "immutable";

const initialState = List([]);

export default (state = initialState, action) => {
    switch (action.type) {
    case Actions.ADD_TEAMS:
        return state.merge(action.teams);
    default:
        return state;
    }
};
