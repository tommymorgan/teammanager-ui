import Immutable from "immutable";
import {
    LOCATION_CHANGE,
} from "react-router-redux";

const initialState = Immutable.fromJS({});

export default (state = initialState, action) => {
    switch (action.type) {
    case LOCATION_CHANGE:
        return state.merge({
            routing: action.payload,
        });
    default:
        return state;
    }
};
