import {
    get as $get,
    post as $post,
} from "jquery";

export const get = url => $get(url);

export const post = (url, data = {}) => $post({
    url,
    data: JSON.stringify(data),
    dataType: "json",
    headers: {
        "Content-Type": "application/json",
    },
});
