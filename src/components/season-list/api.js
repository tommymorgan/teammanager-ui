import { get } from "../../http";

export const getSeasons = () => get("http://teammanager.local/api/seasons");
