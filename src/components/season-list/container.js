import * as SeasonsApi from "./api";
import { connect } from "react-redux";
import React from "react";
import * as SeasonsActions from "../../redux/actions/seasons";
import SeasonList from "./season-list";

class Container extends React.Component {
    componentWillMount() {
        this.props.getSeasons();
    }

    render() {
        return <SeasonList {...this.props}/>;
    }
}

Container.propTypes = {
    getSeasons: React.PropTypes.func.isRequired,
};

export const mapStateToProps = (state) => {
    const seasons = state.seasons.toJS();

    return {
        seasons,
    };
};

export const mapDispatchToProps = (dispatch) => {
    return {
        getSeasons: () => dispatch(SeasonsActions.getSeasons(SeasonsApi.getSeasons())),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
