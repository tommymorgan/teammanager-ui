import {
    Link,
} from "react-router";
import React from "react";
import {
    seasons as seasonsPropType,
} from "../prop-types";

const season = (s) =>
    <li key={s.id}>
        <Link to={`season/${s.id}`}>{s.name}</Link>
    </li>;

const renderEmpty = (seasons) => {
    if (seasons.length) {
        return "";
    }

    return <li>{"There are no seasons"}</li>;
};

const Seasons = ({ seasons }) => // eslint-disable-line comma-dangle
    <ul>
        {seasons.map(season)}
        {renderEmpty(seasons)}
    </ul>;

Seasons.displayName = "Seasons";
Seasons.propTypes = {
    seasons: seasonsPropType,
};
Seasons.defaultProps = {
    seasons: [],
};

export default Seasons;
