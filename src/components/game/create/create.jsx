import React from "react";
import { teams as teamsPropType } from "../../prop-types";

const CreateGame = ({teams, awayTeamId, homeTeamId, onAwayTeamChange, onCreate, onHomeTeamChange}) =>
    <section>
        <div>
            <label>
                {"Away"}
                <select
                    className="away-team"
                    defaultValue={awayTeamId}
                    onChange={onAwayTeamChange}
                >
                    {teams.map(t =>
                        <option
                            key={`away-team-${t.id}`}
                            value={t.id}
                        >
                            {t.name}
                        </option>
                    )}
                </select>
            </label>

            <label>
                {"Home"}
                <select
                    className="home-team"
                    defaultValue={homeTeamId}
                    onChange={onHomeTeamChange}
                >
                    {teams.map(t =>
                        <option
                            key={`home-team-${t.id}`}
                            value={t.id}
                        >
                            {t.name}
                        </option>
                    )}
                </select>
            </label>
        </div>
        <button
            onClick={onCreate(awayTeamId, homeTeamId)}
        >
            {"Create"}
        </button>
    </section>;

CreateGame.displayName = "CreateGame";
CreateGame.propTypes = {
    awayTeamId: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string,
    ]).isRequired,
    homeTeamId: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string,
    ]).isRequired,
    onAwayTeamChange: React.PropTypes.func.isRquired,
    onCreate: React.PropTypes.func.isRquired,
    onHomeTeamChange: React.PropTypes.func.isRquired,
    teams: teamsPropType,
};

export default CreateGame;
