import { connect } from "react-redux";
import React from "react";

export const mapStateToProps = (state) => {
    const teams = state.teams.toJS();

    return {
        teams,
    };
};

export default connect(mapStateToProps)(<div/>);
