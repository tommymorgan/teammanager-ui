import React from "react";

const TeamManager = ({children}) => // eslint-disable-line comma-dangle
    <div>
        {children}
    </div>;

TeamManager.displayName = "TeamManager";
TeamManager.propTypes = {
    children: React.PropTypes.object,
};

export default TeamManager;
