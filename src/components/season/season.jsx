import React from "react";
import { Link } from "react-router";
import { season as seasonPropType } from "../prop-types";
import Teams from "./teams";
import CreateGame from "../game/create/create";

const Season = ({season, onAwayTeamChange, onCreateGame, onHomeTeamChange}) => { // eslint-disable-line comma-dangle
    return (
        <section>
            {season.id &&
                <div>
                    <h1>{season.name}</h1>

                    <Teams teams={season.teams}/>

                    {season.createGame.awayTeamId && season.createGame.homeTeamId &&
                        <CreateGame
                            awayTeamId={season.createGame.awayTeamId}
                            homeTeamId={season.createGame.homeTeamId}
                            onAwayTeamChange={onAwayTeamChange}
                            onCreate={onCreateGame}
                            onHomeTeamChange={onHomeTeamChange}
                            teams={season.teams}
                        />
                    }

                    <Link to={`/season/${season.id}/players`}>{"Players"}</Link>
                </div>
            }
        </section>
    );
};

Season.displayName = "Season";
Season.propTypes = {
    onAwayTeamChange: React.PropTypes.func.isRquired,
    onCreateGame: React.PropTypes.func.isRequired,
    onHomeTeamChange: React.PropTypes.func.isRquired,
    season: seasonPropType.isRequired,
};

export default Season;
