import {
    get,
    post,
} from "../../http";

export const createGame = (awayTeamId, homeTeamId) => post("http://teammanager.local/api/games", {
    awayTeamId,
    homeTeamId,
});
export const getSeason = id => get(`http://teammanager.local/api/seasons/${id}/dto`);
