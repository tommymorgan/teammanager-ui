import React from "react";
import { Link } from "react-router";
import { team as teamPropType } from "../prop-types";

const team = t => {
    return (
        <li key={t.id}>
            <Link to={`/team/${t.id}`}>{t.name}</Link>
        </li>
    );
};

const renderEmpty = teams => {
    if (teams.length) {
        return "";
    }

    return <li>{"There are no teams"}</li>;
};

const Teams = ({ teams }) => { // eslint-disable-line comma-dangle
    return (
        <ul>
            {teams.map(t => team(t))}
            {renderEmpty(teams)}
        </ul>
    );
};

Teams.displayName = "Teams";
Teams.propTypes = {
    teams: React.PropTypes.arrayOf(teamPropType),
};
Teams.defaultProps = {
    teams: [],
};

export default Teams;
