import { connect } from "react-redux";
import React from "react";
import * as SeasonActions from "../../redux/actions/season";
import Season from "./season";
import * as SeasonApi from "./api";
import * as propTypes from "../prop-types";

class Container extends React.Component {
    componentWillMount() {
        this.props.getSeason();
    }

    render() {
        if (!this.props.season || this.props.season.id <= 0) {
            return null;
        }

        return <Season {...this.props}/>;
    }
}

Container.propTypes = {
    getSeason: React.PropTypes.func.isRequired,
    season: propTypes.season,
};

export const mapStateToProps = (state) => {
    const season = state.season.toJS();

    return {
        season,
    };
};

export const mapDispatchToProps = (dispatch, props) => {
    return {
        getSeason: () => dispatch(SeasonActions.getSeason(SeasonApi.getSeason(props.params.id))),
        onAwayTeamChange: ev => {
            const id = ev.target.value;
            dispatch(SeasonActions.setAwayTeamId(id));
        },
        onCreateGame: (awayTeamId, homeTeamId) => () => {
            SeasonApi.createGame(awayTeamId, homeTeamId);
        },
        onHomeTeamChange: ev => {
            const id = ev.target.value;
            dispatch(SeasonActions.setHomeTeamId(id));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
