import React from "react";
import {
    SeasonRecord,
    TeamRecord,
} from "../records";

export const team = React.PropTypes.shape(TeamRecord);

export const teams = React.PropTypes.arrayOf(React.PropTypes.shape(TeamRecord));

export const season = React.PropTypes.shape(SeasonRecord);

export const seasons = React.PropTypes.arrayOf(SeasonRecord);
