import {
    List,
	Record,
} from "immutable";

export const GameRecord = Record({
    id: null,
    awayTeamId: -1,
    homeTeamId: -1,
});

export const TeamRecord = Record({
    id: -1,
    name: "",
});

export const SeasonRecord = Record({
    id: -1,
    createGame: GameRecord,
    name: "",
    teams: List(TeamRecord),
});
